﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication8
{
    public partial class FormContact : Form
    {
        Core.Contacts myContacts;
        int ID = -1;
        BindingSource bs = new BindingSource();


        public FormContact(Core.Contacts myContacts)
        {
            InitializeComponent();
            this.myContacts = myContacts;
            this.pictureBox1.ImageLocation = @"images\index.png";
            this.pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            bs.DataSource = this.myContacts.groups;
            this.comboBox1.DataSource = bs;
            this.comboBox1.DisplayMember = "groupName";
            this.comboBox1.ValueMember = "ID";
            this.comboBox1.DataBindings.Add("SelectedValue",bs,"ID");
            this.Text = "Новый контакт";

        }
        public FormContact(Core.Contacts myContacts,int currID)
        {
            InitializeComponent();
            this.myContacts = myContacts;
            this.ID = currID;


            Core.Name currentName = myContacts.SelectByID(currID);
            
            this.pictureBox1.ImageLocation = currentName.photoPath;
            this.pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;

            this.textBoxFirsName.Text = currentName.FirsName;
            this.textBoxLastName.Text = currentName.LastName;
            this.textBoxHomePhoneNumber.Text = currentName.HomePhoneNumber;
            this.textBoxCellPhoneNumber.Text = currentName.CellPhoneNumber;
            this.textBoxphotoPath.Text = currentName.photoPath;
            bs.DataSource = this.myContacts.groups;
            this.comboBox1.DataSource = bs;
            this.comboBox1.DisplayMember = "groupName";
            this.comboBox1.ValueMember = "ID";
            this.comboBox1.SelectedValue = currentName.groupID;
            this.Text = "Изменение контакта";


        }

        bool NameInputCheck(TextBox textBox)
        {
            Regex regex = new Regex(@"^[\p{L}'.-]+$");

            if ( !regex.IsMatch(textBox.Text) )
            {
                textBox.ForeColor = Color.Red;
                DialogResult result = MessageBox.Show("Имя или фамилия должны состоять только из букв",
                     "Ошибка ввода",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error,
                     MessageBoxDefaultButton.Button2);
                
                return false;
            }
            textBox.ForeColor = Color.Black;
            return true;
        }
        private void buttonSave_Click(object sender,EventArgs e)
        {

            

            if ( this.textBoxFirsName.Text.Length < 1 )
            {
                this.labelFirstName.Text = "Надо указать имя";
                this.labelFirstName.ForeColor = Color.Red;
                
                return;
            }
            else 
            {
                if ( !NameInputCheck(this.textBoxFirsName) )
                {
                    return;
                }

                this.labelFirstName.Text = "Имя";
                this.labelFirstName.ForeColor = Color.Black;
            }


            if ( this.textBoxLastName.Text.Length < 1 )
            {
                this.labelLastName.Text = "Надо указать фамилию";
                this.labelLastName.ForeColor = Color.Red;
                 return;
            }
            else
            {
                if ( !NameInputCheck(this.textBoxLastName))
                {
                    return;
                }
                this.labelLastName.Text = "Фамилия";
                this.labelLastName.ForeColor = Color.Black;
            }
            Regex regex = new Regex(@"^\+?[0-9]{3}-?[0-9]{2,12}$");
            bool a = regex.IsMatch(this.textBoxHomePhoneNumber.Text);
            if ( !regex.IsMatch(this.textBoxHomePhoneNumber.Text) )
            {
                this.labelHomePhoneNumber.Text = "Надо указать телефон";
                this.labelHomePhoneNumber.ForeColor = Color.Red;
                return;
            }
            else if ( !regex.IsMatch(this.textBoxCellPhoneNumber.Text) )
            {
                this.labelCellPhoneNumber.Text = "Надо указать телефон";
                this.labelCellPhoneNumber.ForeColor = Color.Red;
                return;
            }
            if ( ID == -1 )
            {
                myContacts.CreateName(FillName());
            }
            else
            {
                myContacts.UpdateName(FillName(),this.ID);
            }
            this.DialogResult = DialogResult.Yes;
        }
        Core.Name FillName()
        {
            Core.Name name = new Core.Name();
            name.FirsName = this.textBoxFirsName.Text;
            name.LastName = this.textBoxLastName.Text;
            name.HomePhoneNumber = this.textBoxHomePhoneNumber.Text;
            name.CellPhoneNumber = this.textBoxCellPhoneNumber.Text;
            name.groupID = Convert.ToInt32(this.comboBox1.SelectedValue);
            name.photoPath = this.textBoxphotoPath.Text;
            if ( String.IsNullOrEmpty(this.textBoxphotoPath.Text) )
            {
                name.photoPath = @"images\index.png";
            }
            return name;
        }

        private void button1_Click(object sender,EventArgs e)
        {
            this.openFileDialog1 = new OpenFileDialog();
            this.openFileDialog1.Filter = "Фото (*.BMP;*.JPG;*.PNG)|*.BMP;*.JPG;*.PNG";
            openFileDialog1.FilterIndex = 1;
            if ( openFileDialog1.ShowDialog() == DialogResult.OK )
            {
                FileInfo f = new FileInfo(openFileDialog1.FileName);
                if ( f.Length > 20000000 )
                {
                    DialogResult result = MessageBox.Show("Попробуйте выбрать рисунок поменьше",
                      "Рисунок слишком велик",
                      MessageBoxButtons.OK,
                      MessageBoxIcon.Error,
                      MessageBoxDefaultButton.Button2);
                    return;
                }
                this.textBoxphotoPath.Text = openFileDialog1.FileName;
                this.pictureBox1.ImageLocation = this.textBoxphotoPath.Text;
                this.pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            }

        }
    }
}
