﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class Name
    {
     
        public int ID
        {
            set; get;
        }
        [DisplayName("Имя") ]
        public string FirsName {
            set; get; }

        [DisplayName("Фамилия")]
        public string LastName
        {
            set; get;
        }

        public int groupID;

        [DisplayName("Группа")]
        public string GroupName
        {
            set; get;
        }

        [DisplayName("Домашний")]
        public string HomePhoneNumber
        {
            set; get;
        }
        [DisplayName("Мобильный")]
        public string CellPhoneNumber
        {
            set; get;
        }

        public string photoPath
        {
            set; get;
        }
    }
}
