﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using Core;


namespace WindowsFormsApplication8
{

    public partial class MainForm : Form
    {
        Contacts myContacts = Contacts.Instance;
        BindingSource bindingSource = new BindingSource();

        public MainForm()
        {
            InitializeComponent();
            myContacts.Load();
            bindingSource.DataSource = myContacts.names;
            this.dataGridView1.DataSource = bindingSource;
            UpdateDatagrid();
            FillTreeView();

        }
        private void treeView1_AfterSelect(object sender,TreeViewEventArgs e)
        {
            TreeView treeView = sender as TreeView;
            TreeNode node = e.Node;
            if ( treeView.SelectedNode.Level == 0 )
            {
                return;
            }
            if ( treeView.SelectedNode.Level == 2 )
            {
                node = e.Node.Parent;
            }

            int currID = Convert.ToInt32(node.Name);

            Name name = myContacts.SelectByID(currID);

            this.textBoxFirstName.Text = name.FirsName;
            this.textBoxLastName.Text = name.LastName;
            this.textBoxHomePhoneNumber.Text = name.HomePhoneNumber;
            this.textBoxCellPhoneNumber.Text = name.CellPhoneNumber;
            this.textBoxGroupName.Text = name.GroupName;
            this.textBoxID.Text = name.ID.ToString();
            this.pictureBox1.ImageLocation = name.photoPath;
            this.pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;

        }
       
        private void dataGridView1_CellClick(object sender,DataGridViewCellEventArgs e)
        {
            if ( e.RowIndex >= 0 )
            {
               
                DataGridViewRow currRow = this.dataGridView1.Rows [e.RowIndex];
                if ( currRow.Cells ["firsName"].Value == null )
                {
                    return;
                }
                this.textBoxFirstName.Text = currRow.Cells ["firsName"].Value.ToString();
                this.textBoxLastName.Text = currRow.Cells ["LastName"].Value.ToString();
                this.textBoxHomePhoneNumber.Text = currRow.Cells ["HomePhoneNumber"].Value.ToString();
                this.textBoxCellPhoneNumber.Text = currRow.Cells ["CellPhoneNumber"].Value.ToString();
                this.textBoxGroupName.Text = currRow.Cells ["GroupName"].Value.ToString();
                this.textBoxID.Text = currRow.Cells ["ID"].Value.ToString();
                this.pictureBox1.ImageLocation = currRow.Cells ["photoPath"].Value.ToString();
                this.pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            }

        }

        private void FillTreeView()
        {
            this.treeView1.Nodes.Clear();
            foreach ( var item in myContacts.groups )
            {

                List<Name> listNames = myContacts.SelectNamesByGroupID(item.ID);

                TreeNode nodeGroup = new TreeNode(item.groupName,item.ID,item.ID);
                foreach ( var i in listNames )
                {
                    TreeNode nodeName = new TreeNode();
                    nodeName.Text = i.FirsName + " " + i.LastName;
                    nodeName.Name = i.ID.ToString();
                    nodeGroup.Nodes.Add(nodeName);
                }

                treeView1.Nodes.Add(nodeGroup);
            }


        }
        private void buttonCreate_Click(object sender,EventArgs e)
        {
            FormContact f2 = new FormContact(myContacts);
            DialogResult dr = f2.ShowDialog();

            if ( dr == DialogResult.Yes )
            {
                myContacts.Load();
                UpdateDatagrid();
                FillTreeView();
            }




        }

        private void buttonUpdate_Click(object sender,EventArgs e)
        {
            FormContact f2 = new FormContact(myContacts,Convert.ToInt32(this.textBoxID.Text));
            DialogResult dr = f2.ShowDialog();
            if ( dr == DialogResult.Yes )
            {
                myContacts.Load();
                UpdateDatagrid();
                FillTreeView();
            }

        }
        private void UpdateDatagrid()
        {
            myContacts.Load();
            this.bindingSource.EndEdit();
            this.bindingSource.ResetBindings(true);
            this.bindingSource.DataSource = myContacts.names;
            this.dataGridView1.DataSource = this.bindingSource;
            dataGridView1.Columns ["ID"].Visible = false;
            dataGridView1.Columns ["HomePhoneNumber"].Visible = false;
            dataGridView1.Columns ["CellPhoneNumber"].Visible = false;
            dataGridView1.Columns ["photoPath"].Visible = false;

            if ( this.dataGridView1.Rows.Count != 0 )
            {
               
                DataGridViewCellEventArgs e = new DataGridViewCellEventArgs(0,0);
                dataGridView1_CellClick(new object(),e);

            }

        }
        private void buttonDelete_Click(object sender,EventArgs e)
        {
            Core.Name currentName = myContacts.SelectByID(Convert.ToInt32(this.textBoxID.Text));

            DialogResult result = MessageBox.Show("Удалить контакт?",
            currentName.FirsName + " " +
            currentName.LastName,
            MessageBoxButtons.YesNo,
            MessageBoxIcon.Warning,
            MessageBoxDefaultButton.Button2);

            if ( result == DialogResult.Yes )
            {
                dataGridView1.DataSource = null;
                myContacts.DeleteName(Convert.ToInt32(this.textBoxID.Text));
                myContacts.Load();
                UpdateDatagrid();
                FillTreeView();

            }
           

        }

        private void button1_Click_1(object sender,EventArgs e)
        {
            this.dataGridView1.Visible = false;
            this.treeView1.Visible = true;
            this.textBoxFilter.Enabled = false;
        }

        private void button2_Click_1(object sender,EventArgs e)
        {
            this.dataGridView1.Visible = true;
            this.treeView1.Visible = false;
            this.textBoxFilter.Enabled = true;
        }

        private void textBoxFilter_TextChanged(object sender,EventArgs e)
        {
            UpdateDatagrid();
            this.bindingSource.EndEdit();
            this.bindingSource.ResetBindings(true);

            List<Name> listNames = this.myContacts.FilteredByName(((TextBox)sender).Text);

            if ( listNames.Count() == 0 )
            {
                dataGridView1.Rows.Clear();
                return;
            }
            this.bindingSource.DataSource = listNames;
            this.dataGridView1.DataSource = this.bindingSource;
            dataGridView1.Columns ["ID"].Visible = false;
            dataGridView1.Columns ["HomePhoneNumber"].Visible = false;
            dataGridView1.Columns ["CellPhoneNumber"].Visible = false;
            dataGridView1.Columns ["photoPath"].Visible = false;

            if ( this.dataGridView1.Rows.Count != 0 )
            {
                DataGridViewCellEventArgs e1 = new DataGridViewCellEventArgs(0,0);
                dataGridView1_CellClick(new object(),e1);

            }
        }
    }
}
