﻿namespace WindowsFormsApplication8
{
    partial class FormContact
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormContact));
            this.textBoxFirsName = new System.Windows.Forms.TextBox();
            this.textBoxLastName = new System.Windows.Forms.TextBox();
            this.textBoxHomePhoneNumber = new System.Windows.Forms.TextBox();
            this.textBoxCellPhoneNumber = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.labelFirstName = new System.Windows.Forms.Label();
            this.labelGroupName = new System.Windows.Forms.Label();
            this.labelCellPhoneNumber = new System.Windows.Forms.Label();
            this.labelHomePhoneNumber = new System.Windows.Forms.Label();
            this.labelLastName = new System.Windows.Forms.Label();
            this.buttonPhoto = new System.Windows.Forms.Button();
            this.textBoxphotoPath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxFirsName
            // 
            this.textBoxFirsName.Location = new System.Drawing.Point(334, 13);
            this.textBoxFirsName.Name = "textBoxFirsName";
            this.textBoxFirsName.Size = new System.Drawing.Size(154, 20);
            this.textBoxFirsName.TabIndex = 0;
            // 
            // textBoxLastName
            // 
            this.textBoxLastName.Location = new System.Drawing.Point(334, 55);
            this.textBoxLastName.Name = "textBoxLastName";
            this.textBoxLastName.Size = new System.Drawing.Size(154, 20);
            this.textBoxLastName.TabIndex = 1;
            // 
            // textBoxHomePhoneNumber
            // 
            this.textBoxHomePhoneNumber.Location = new System.Drawing.Point(334, 99);
            this.textBoxHomePhoneNumber.Name = "textBoxHomePhoneNumber";
            this.textBoxHomePhoneNumber.Size = new System.Drawing.Size(154, 20);
            this.textBoxHomePhoneNumber.TabIndex = 2;
            // 
            // textBoxCellPhoneNumber
            // 
            this.textBoxCellPhoneNumber.Location = new System.Drawing.Point(334, 144);
            this.textBoxCellPhoneNumber.Name = "textBoxCellPhoneNumber";
            this.textBoxCellPhoneNumber.Size = new System.Drawing.Size(154, 20);
            this.textBoxCellPhoneNumber.TabIndex = 3;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(334, 189);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(154, 21);
            this.comboBox1.TabIndex = 4;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(13, 13);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(292, 273);
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(413, 263);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 6;
            this.buttonCancel.Text = "Отмена";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(332, 263);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 23);
            this.buttonSave.TabIndex = 7;
            this.buttonSave.Text = "Сохранить";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // labelFirstName
            // 
            this.labelFirstName.AutoSize = true;
            this.labelFirstName.Location = new System.Drawing.Point(512, 20);
            this.labelFirstName.Name = "labelFirstName";
            this.labelFirstName.Size = new System.Drawing.Size(29, 13);
            this.labelFirstName.TabIndex = 8;
            this.labelFirstName.Text = "Имя";
            // 
            // labelGroupName
            // 
            this.labelGroupName.AutoSize = true;
            this.labelGroupName.Location = new System.Drawing.Point(512, 192);
            this.labelGroupName.Name = "labelGroupName";
            this.labelGroupName.Size = new System.Drawing.Size(42, 13);
            this.labelGroupName.TabIndex = 9;
            this.labelGroupName.Text = "Группа";
            // 
            // labelCellPhoneNumber
            // 
            this.labelCellPhoneNumber.AutoSize = true;
            this.labelCellPhoneNumber.Location = new System.Drawing.Point(512, 147);
            this.labelCellPhoneNumber.Name = "labelCellPhoneNumber";
            this.labelCellPhoneNumber.Size = new System.Drawing.Size(66, 13);
            this.labelCellPhoneNumber.TabIndex = 10;
            this.labelCellPhoneNumber.Text = "Мобильный";
            // 
            // labelHomePhoneNumber
            // 
            this.labelHomePhoneNumber.AutoSize = true;
            this.labelHomePhoneNumber.Location = new System.Drawing.Point(512, 102);
            this.labelHomePhoneNumber.Name = "labelHomePhoneNumber";
            this.labelHomePhoneNumber.Size = new System.Drawing.Size(62, 13);
            this.labelHomePhoneNumber.TabIndex = 11;
            this.labelHomePhoneNumber.Text = "Домашний";
            // 
            // labelLastName
            // 
            this.labelLastName.AutoSize = true;
            this.labelLastName.Location = new System.Drawing.Point(512, 58);
            this.labelLastName.Name = "labelLastName";
            this.labelLastName.Size = new System.Drawing.Size(56, 13);
            this.labelLastName.TabIndex = 12;
            this.labelLastName.Text = "Фамилия";
            // 
            // buttonPhoto
            // 
            this.buttonPhoto.Image = ((System.Drawing.Image)(resources.GetObject("buttonPhoto.Image")));
            this.buttonPhoto.Location = new System.Drawing.Point(413, 225);
            this.buttonPhoto.Name = "buttonPhoto";
            this.buttonPhoto.Size = new System.Drawing.Size(75, 23);
            this.buttonPhoto.TabIndex = 13;
            this.buttonPhoto.UseVisualStyleBackColor = true;
            this.buttonPhoto.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBoxphotoPath
            // 
            this.textBoxphotoPath.Location = new System.Drawing.Point(718, 448);
            this.textBoxphotoPath.Name = "textBoxphotoPath";
            this.textBoxphotoPath.Size = new System.Drawing.Size(154, 20);
            this.textBoxphotoPath.TabIndex = 14;
            this.textBoxphotoPath.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(512, 230);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Фото";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Linen;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(638, 304);
            this.ControlBox = false;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxphotoPath);
            this.Controls.Add(this.buttonPhoto);
            this.Controls.Add(this.labelLastName);
            this.Controls.Add(this.labelHomePhoneNumber);
            this.Controls.Add(this.labelCellPhoneNumber);
            this.Controls.Add(this.labelGroupName);
            this.Controls.Add(this.labelFirstName);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.textBoxCellPhoneNumber);
            this.Controls.Add(this.textBoxHomePhoneNumber);
            this.Controls.Add(this.textBoxLastName);
            this.Controls.Add(this.textBoxFirsName);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Контакт";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxFirsName;
        private System.Windows.Forms.TextBox textBoxLastName;
        private System.Windows.Forms.TextBox textBoxHomePhoneNumber;
        private System.Windows.Forms.TextBox textBoxCellPhoneNumber;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Label labelFirstName;
        private System.Windows.Forms.Label labelGroupName;
        private System.Windows.Forms.Label labelCellPhoneNumber;
        private System.Windows.Forms.Label labelHomePhoneNumber;
        private System.Windows.Forms.Label labelLastName;
        private System.Windows.Forms.Button buttonPhoto;
        private System.Windows.Forms.TextBox textBoxphotoPath;
        private System.Windows.Forms.Label label1;
    }
}