﻿namespace WindowsFormsApplication8
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.textBoxFirstName = new System.Windows.Forms.TextBox();
            this.textBoxLastName = new System.Windows.Forms.TextBox();
            this.textBoxHomePhoneNumber = new System.Windows.Forms.TextBox();
            this.textBoxCellPhoneNumber = new System.Windows.Forms.TextBox();
            this.textBoxGroupName = new System.Windows.Forms.TextBox();
            this.buttonCreate = new System.Windows.Forms.Button();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.textBoxID = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labelLastName = new System.Windows.Forms.Label();
            this.labelHomePhoneNumber = new System.Windows.Forms.Label();
            this.labelCellPhoneNumber = new System.Windows.Forms.Label();
            this.labelGroupName = new System.Windows.Forms.Label();
            this.labelFirstName = new System.Windows.Forms.Label();
            this.buttonOnTreeView = new System.Windows.Forms.Button();
            this.buttonOnDataGrid = new System.Windows.Forms.Button();
            this.textBoxFilter = new System.Windows.Forms.TextBox();
            this.labelFilter = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(12, 38);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(615, 464);
            this.treeView1.TabIndex = 0;
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 38);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(615, 464);
            this.dataGridView1.TabIndex = 4;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // textBoxFirstName
            // 
            this.textBoxFirstName.Enabled = false;
            this.textBoxFirstName.Location = new System.Drawing.Point(642, 38);
            this.textBoxFirstName.Name = "textBoxFirstName";
            this.textBoxFirstName.Size = new System.Drawing.Size(184, 20);
            this.textBoxFirstName.TabIndex = 5;
            // 
            // textBoxLastName
            // 
            this.textBoxLastName.Enabled = false;
            this.textBoxLastName.Location = new System.Drawing.Point(642, 64);
            this.textBoxLastName.Name = "textBoxLastName";
            this.textBoxLastName.Size = new System.Drawing.Size(184, 20);
            this.textBoxLastName.TabIndex = 6;
            // 
            // textBoxHomePhoneNumber
            // 
            this.textBoxHomePhoneNumber.Enabled = false;
            this.textBoxHomePhoneNumber.Location = new System.Drawing.Point(642, 90);
            this.textBoxHomePhoneNumber.Name = "textBoxHomePhoneNumber";
            this.textBoxHomePhoneNumber.Size = new System.Drawing.Size(184, 20);
            this.textBoxHomePhoneNumber.TabIndex = 7;
            // 
            // textBoxCellPhoneNumber
            // 
            this.textBoxCellPhoneNumber.Enabled = false;
            this.textBoxCellPhoneNumber.Location = new System.Drawing.Point(642, 116);
            this.textBoxCellPhoneNumber.Name = "textBoxCellPhoneNumber";
            this.textBoxCellPhoneNumber.Size = new System.Drawing.Size(184, 20);
            this.textBoxCellPhoneNumber.TabIndex = 8;
            // 
            // textBoxGroupName
            // 
            this.textBoxGroupName.Enabled = false;
            this.textBoxGroupName.Location = new System.Drawing.Point(642, 142);
            this.textBoxGroupName.Name = "textBoxGroupName";
            this.textBoxGroupName.Size = new System.Drawing.Size(184, 20);
            this.textBoxGroupName.TabIndex = 9;
            // 
            // buttonCreate
            // 
            this.buttonCreate.Location = new System.Drawing.Point(642, 421);
            this.buttonCreate.Name = "buttonCreate";
            this.buttonCreate.Size = new System.Drawing.Size(118, 23);
            this.buttonCreate.TabIndex = 10;
            this.buttonCreate.Text = "Новый контакт";
            this.buttonCreate.UseVisualStyleBackColor = true;
            this.buttonCreate.Click += new System.EventHandler(this.buttonCreate_Click);
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Location = new System.Drawing.Point(642, 450);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(118, 23);
            this.buttonUpdate.TabIndex = 11;
            this.buttonUpdate.Text = "Изменить контакт";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Location = new System.Drawing.Point(642, 479);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(118, 23);
            this.buttonDelete.TabIndex = 12;
            this.buttonDelete.Text = "Удалить контакт";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // textBoxID
            // 
            this.textBoxID.Location = new System.Drawing.Point(642, 12);
            this.textBoxID.Name = "textBoxID";
            this.textBoxID.Size = new System.Drawing.Size(100, 20);
            this.textBoxID.TabIndex = 13;
            this.textBoxID.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(642, 168);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(267, 244);
            this.pictureBox1.TabIndex = 14;
            this.pictureBox1.TabStop = false;
            // 
            // labelLastName
            // 
            this.labelLastName.AutoSize = true;
            this.labelLastName.Location = new System.Drawing.Point(843, 67);
            this.labelLastName.Name = "labelLastName";
            this.labelLastName.Size = new System.Drawing.Size(56, 13);
            this.labelLastName.TabIndex = 20;
            this.labelLastName.Text = "Фамилия";
            // 
            // labelHomePhoneNumber
            // 
            this.labelHomePhoneNumber.AutoSize = true;
            this.labelHomePhoneNumber.Location = new System.Drawing.Point(843, 93);
            this.labelHomePhoneNumber.Name = "labelHomePhoneNumber";
            this.labelHomePhoneNumber.Size = new System.Drawing.Size(62, 13);
            this.labelHomePhoneNumber.TabIndex = 19;
            this.labelHomePhoneNumber.Text = "Домашний";
            // 
            // labelCellPhoneNumber
            // 
            this.labelCellPhoneNumber.AutoSize = true;
            this.labelCellPhoneNumber.Location = new System.Drawing.Point(843, 119);
            this.labelCellPhoneNumber.Name = "labelCellPhoneNumber";
            this.labelCellPhoneNumber.Size = new System.Drawing.Size(66, 13);
            this.labelCellPhoneNumber.TabIndex = 18;
            this.labelCellPhoneNumber.Text = "Мобильный";
            // 
            // labelGroupName
            // 
            this.labelGroupName.AutoSize = true;
            this.labelGroupName.Location = new System.Drawing.Point(843, 145);
            this.labelGroupName.Name = "labelGroupName";
            this.labelGroupName.Size = new System.Drawing.Size(42, 13);
            this.labelGroupName.TabIndex = 17;
            this.labelGroupName.Text = "Группа";
            // 
            // labelFirstName
            // 
            this.labelFirstName.AutoSize = true;
            this.labelFirstName.Location = new System.Drawing.Point(843, 41);
            this.labelFirstName.Name = "labelFirstName";
            this.labelFirstName.Size = new System.Drawing.Size(29, 13);
            this.labelFirstName.TabIndex = 16;
            this.labelFirstName.Text = "Имя";
            // 
            // buttonOnTreeView
            // 
            this.buttonOnTreeView.Location = new System.Drawing.Point(12, 9);
            this.buttonOnTreeView.Name = "buttonOnTreeView";
            this.buttonOnTreeView.Size = new System.Drawing.Size(75, 23);
            this.buttonOnTreeView.TabIndex = 21;
            this.buttonOnTreeView.Text = "В группах";
            this.buttonOnTreeView.UseVisualStyleBackColor = true;
            this.buttonOnTreeView.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // buttonOnDataGrid
            // 
            this.buttonOnDataGrid.Location = new System.Drawing.Point(93, 9);
            this.buttonOnDataGrid.Name = "buttonOnDataGrid";
            this.buttonOnDataGrid.Size = new System.Drawing.Size(75, 23);
            this.buttonOnDataGrid.TabIndex = 22;
            this.buttonOnDataGrid.Text = "Списком";
            this.buttonOnDataGrid.UseVisualStyleBackColor = true;
            this.buttonOnDataGrid.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // textBoxFilter
            // 
            this.textBoxFilter.Location = new System.Drawing.Point(328, 12);
            this.textBoxFilter.Name = "textBoxFilter";
            this.textBoxFilter.Size = new System.Drawing.Size(298, 20);
            this.textBoxFilter.TabIndex = 23;
            this.textBoxFilter.TextChanged += new System.EventHandler(this.textBoxFilter_TextChanged);
            // 
            // labelFilter
            // 
            this.labelFilter.AutoSize = true;
            this.labelFilter.Location = new System.Drawing.Point(275, 15);
            this.labelFilter.Name = "labelFilter";
            this.labelFilter.Size = new System.Drawing.Size(47, 13);
            this.labelFilter.TabIndex = 24;
            this.labelFilter.Text = "Фильтр";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(922, 519);
            this.Controls.Add(this.labelFilter);
            this.Controls.Add(this.textBoxFilter);
            this.Controls.Add(this.buttonOnDataGrid);
            this.Controls.Add(this.buttonOnTreeView);
            this.Controls.Add(this.labelLastName);
            this.Controls.Add(this.labelHomePhoneNumber);
            this.Controls.Add(this.labelCellPhoneNumber);
            this.Controls.Add(this.labelGroupName);
            this.Controls.Add(this.labelFirstName);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.textBoxID);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.buttonUpdate);
            this.Controls.Add(this.buttonCreate);
            this.Controls.Add(this.textBoxGroupName);
            this.Controls.Add(this.textBoxCellPhoneNumber);
            this.Controls.Add(this.textBoxHomePhoneNumber);
            this.Controls.Add(this.textBoxLastName);
            this.Controls.Add(this.textBoxFirstName);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.treeView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Менеджер контактов";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox textBoxFirstName;
        private System.Windows.Forms.TextBox textBoxLastName;
        private System.Windows.Forms.TextBox textBoxHomePhoneNumber;
        private System.Windows.Forms.TextBox textBoxCellPhoneNumber;
        private System.Windows.Forms.TextBox textBoxGroupName;
        private System.Windows.Forms.Button buttonCreate;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.TextBox textBoxID;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label labelLastName;
        private System.Windows.Forms.Label labelHomePhoneNumber;
        private System.Windows.Forms.Label labelCellPhoneNumber;
        private System.Windows.Forms.Label labelGroupName;
        private System.Windows.Forms.Label labelFirstName;
        private System.Windows.Forms.Button buttonOnTreeView;
        private System.Windows.Forms.Button buttonOnDataGrid;
        private System.Windows.Forms.TextBox textBoxFilter;
        private System.Windows.Forms.Label labelFilter;
    }
}

