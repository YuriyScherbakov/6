﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace Core
{
    public sealed class Contacts
    {
        private static readonly Contacts instance = new Contacts();

        private Contacts()
        {
            this.names = new List<Name>();
            this.groups = new List<Group>();

            Load();
        }

        public List<Name> names;
        public List<Group> groups;

        const string path = "Contacts.xml";

        public static Contacts Instance
        {
            get
            {
                return instance;
            }
        }

        public List<Name> FilteredByName(string startsWithStr)
        {
            List<Name> listNames = (from q in Instance.names
                                   where ( ( q.FirsName.ToLower().StartsWith(startsWithStr.ToLower()) )
                                   ||
                                           ( q.LastName.ToLower().StartsWith(startsWithStr.ToLower()) )
                                           )
                                   select q).ToList();
            return listNames;
        }

        public Name SelectByID(int currID)
        {
            var query = ( from q in Instance.names
                          where q.ID == currID
                          select q ).FirstOrDefault();
            return query;
        }

        public List<Name> SelectNamesByGroupID(int currID)
        {
            List <Name> listNames = ( from q in Instance.names
                          where q.groupID == currID
                          select q ).ToList();
            return listNames;
        }




        public void Load()
        {
            this.names.Clear();
            this.groups.Clear();

            XmlDocument xdoc = new XmlDocument();
            xdoc.Load(Contacts.path);

            XmlNodeList xNonedes = xdoc.SelectNodes("Contacts/Names/Name");

            foreach ( XmlNode xn in xNonedes )
            {

                this.names.Add(new Name
                {
                    ID = Convert.ToInt32(xn.Attributes ["ID"].Value),
                    FirsName = xn ["FirstName"].InnerText,
                    LastName = xn ["LastName"].InnerText,
                    groupID = Convert.ToInt32(xn ["groupID"].InnerText),
                    CellPhoneNumber = xn ["cellPhoneNumber"].InnerText,
                    HomePhoneNumber = xn ["homePhoneNumber"].InnerText,
                    photoPath = xn ["photoPath"].InnerText

                });
              
            }

            xNonedes = xdoc.SelectNodes("/Contacts/Groups/Group");

            foreach ( XmlNode xn in xNonedes )
            {
                this.groups.Add(new Group
                {
                    ID = Convert.ToInt32(xn.Attributes ["ID"].Value),
                    groupName = xn ["groupName"].InnerText,
                    logoPath = xn ["logoPath"].InnerText
                });

            }
            foreach ( Name name in names )
            {
                name.GroupName = ( from gr in groups
                                   where
                 gr.ID == name.groupID
                                   select gr.groupName ).FirstOrDefault();
            }
        }

        public void CreateName(Name name)
        {
            XDocument xdoc = XDocument.Load(path);

            XElement element = XElement.Load(path);
            int newId =
            (element.Element("Names").Elements("Name").Max(l => int.Parse(l.Attribute("ID").Value)))+1;

            xdoc.Element("Contacts").Element("Names").Add(
                new XElement("Name",new XAttribute("ID",newId),
                new XElement("FirstName",name.FirsName),
                new XElement("LastName",name.LastName),
                new XElement("groupID",name.groupID),
                new XElement("cellPhoneNumber",name.CellPhoneNumber),
                new XElement("homePhoneNumber",name.HomePhoneNumber),
                new XElement("photoPath",name.photoPath)
                ));
            xdoc.Save(path);

        }
        public void CreateGroup(Group group)
        {
            XDocument xdoc = XDocument.Load(path);

            int newId = ( Convert.ToInt32(( from id in xdoc
                                      .Descendants("Group")
                                            select id.Attribute("ID").Value ).Max()) ) + 1;

            xdoc.Element("Contacts").Element("Groups").Add(
                new XElement("Group",new XAttribute("ID",newId),
                new XElement("groupName",group.groupName),
                new XElement("logoPath",group.logoPath)
                ));
            xdoc.Save(path);

        }
        public void DeleteName(int id)
        {
            XDocument xdoc = XDocument.Load(path);
            xdoc.Element("Contacts").Element("Names")
                .Elements("Name").Where(x => Convert.ToInt32(
                x.Attribute("ID").Value) == id).FirstOrDefault().Remove();
            xdoc.Save(path);
        }
        public void DeleteGroup(int id)
        {
            XDocument xdoc = XDocument.Load(path);
            xdoc.Element("Contacts").Element("Groups")
                .Elements("Group").Where(x => Convert.ToInt32(
                x.Attribute("ID").Value) == id).FirstOrDefault().Remove();
            xdoc.Save(path);
        }
        public void UpdateName(Name name,int id)
        {
            XDocument xdoc = XDocument.Load(path);

            var query = from q in xdoc.Element("Contacts").Element("Names").Elements("Name")
                        where Convert.ToInt32(q.Attribute("ID").Value) == id
                        select q;
            XElement element = query.FirstOrDefault();

            element.SetElementValue("FirstName",name.FirsName);
            element.SetElementValue("LastName",name.LastName);
            element.SetElementValue("groupID",name.groupID.ToString());
            element.SetElementValue("cellPhoneNumber",name.CellPhoneNumber);
            element.SetElementValue("homePhoneNumber",name.HomePhoneNumber);
            element.SetElementValue("photoPath",name.photoPath);
            xdoc.Save(path);
        }
        public void UpdateGroup(Group group,int id)
        {
            XDocument xdoc = XDocument.Load(path);

            var query = from q in xdoc.Element("Contacts").Element("Groups").Elements("Group")
                        where Convert.ToInt32(q.Attribute("ID").Value) == id
                        select q;
            XElement element = query.FirstOrDefault();

            element.SetElementValue("groupName",group.groupName);
            element.SetElementValue("LastName",group.logoPath);
            xdoc.Save(path);
        }
    }
}
